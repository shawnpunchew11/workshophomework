﻿using System;
using System.Threading;

namespace FizzBuzz
{
    public class FizzBuzz
    {
        private int start, end;
        private string x, y;

        public FizzBuzz(int start, int end, string x, string y)
        {
            this.start = start;
            this.end = end;
            this.x = x;
            this.y = y;
        }

        public string genString()
        {
            string output = "";
            for (var i = start; i <= end; i++)
            {
                if (i % 3 == 0 && i % 5 == 0)
                {
                    output += x + y + " ";
                } else if (i % 3 == 0)
                {
                    output += x + " ";
                } else if (i % 5 == 0)
                {
                    output += y + " ";
                }
                else
                {
                    output += i + " ";
                }
            }

            return output;
        }
    }
}