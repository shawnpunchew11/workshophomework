﻿using System;
using System.IO;
using System.Runtime.CompilerServices;

namespace FizzBuzz
{
    class Program
    {
        static void Main(string[] args)
        {
            string x = "";
            string y = "";
            if (args.Length == 1)
            {
                x = "Fizz";
                y = "Buzz";
            } else if (args.Length == 2)
            {
                x = args[1];
                y = "Buzz";
            }
            else
            {
                x = args[1];
                y = args[2];
            }
            
            System.IO.StreamReader file = new System.IO.StreamReader(args[0]);
            string line;

            while ((line = file.ReadLine()) != null)
            {
                string[] startEnd = line.Split(",");
                FizzBuzz fbObject = new FizzBuzz(int.Parse(startEnd[0]), int.Parse(startEnd[1]), x, y);
                Console.WriteLine(fbObject.genString());
            }
            
        }
    }
}
