﻿using System;
using System.IO;
using System.Linq;

namespace HelloCS
{
    class Program
    {
        static void Main(string[] args)
        {
            String fileName = args[0];
            var wrapper = int.Parse(args[1]);
            string[] text = File.ReadAllLines(fileName);
            string joined = String.Join(" ", text);
            string[] wordArr = joined.Split(" ");

            var output = "";
            for (var i = 0; i < wordArr.Length; i++)
            {
                output += wordArr[i] + " ";
                if ((i + 1) % wrapper == 0)
                {
                    output += '\n';
                }
            }
            
            File.WriteAllText(fileName, output);
        }
    }
}
