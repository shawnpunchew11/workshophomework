import * as fs from 'fs';

function runDb(): void {
    let currentDb : Map<string, string> = initDb();
    switch(process.argv[2]) {
        case "save": {
            save(process.argv[3], process.argv[4], currentDb);
            break;
        }
        case "load": {
            load(process.argv[3], currentDb);
            break;
        }
        case "delete": {
            deleteKV(process.argv[3], currentDb);
            break;
        }
        default: {
            console.log("Error!");
            break;
        }
    }
    exitDb(currentDb);
}

function initDb(): Map<string, string> {
    let kvStore = new Map();
    const keyValuePairs = fs.readFileSync('db.txt', 'utf8')
    let pairArr : Array<string> = keyValuePairs.split('\n');
    console.log(pairArr);
    for (let i of pairArr) {
        if (i == "") {
            continue;
        } else {
            let indivArr: Array<string> = i.split(",");
            let key: string = indivArr[0].toString();
            let value: string = indivArr[1];
            kvStore.set(key, value);
        }
    }
    return kvStore;
}

function save(key : string, value : string, store : Map<string, string>) : void {
    store.set(key, value);
}

function load(key : string, store: Map<string, string>) : void {
    let val = store.get(key);
    if (val === undefined) {
        val = "NULL";
    }
    console.log(val);
}

function deleteKV(key : string, store : Map<string, string>) {
    store.delete(key);
}

function exitDb(store : Map<string, string>) {
    let output : string = "";
    store.forEach((value : string, key : string) => {
        output += key + "," + value + '\n';
    });
    output = output.replace(/\n$/, "");
    fs.writeFileSync('db.txt', output);
}

runDb();