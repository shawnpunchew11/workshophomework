import {FizzBuzz} from "./FizzBuss";

const lineByLine = require('n-readlines');
const liner = new lineByLine(process.argv[2]);
let line;
while (line = liner.next()) {
    let arrayOfNumbers:Array<number> = line.toString().split(",");
    let startNum = +arrayOfNumbers[0];
    let endNum = +arrayOfNumbers[1];
    let fbObject = new FizzBuzz(startNum, endNum);
    console.log(fbObject.genString());
}