class FizzBuzz {
    start: number;
    end: number;
    x: string;
    y: string;

    constructor(start: number, end:number) {
        this.start = start;
        this.end = end;
        if (process.argv.length == 3) {
            this.x = "Fizz";
            this.y = "Buzz";
        } else if (process.argv.length == 4) {
            this.x = process.argv[3];
            this.y = "Buzz";
        } else if (process.argv.length > 4) {
            this.x = process.argv[3];
            this.y = process.argv[4];
        } else {
            console.log("Invalid Argument");
        }
    }

    genString(): string {
        let outputString:string = "";
        for (let i = this.start; i <= this.end; i++) {
            if (i % 3 == 0 && i % 5 == 0) {
                outputString += this.x + this.y + " ";
            } else if (i % 3 == 0)  {
                outputString += this.x + " ";
            } else if (i % 5 == 0) {
                outputString += this.y + " ";
            } else {
                outputString += i + " ";
            }
        }
        return outputString;
    }
}

export {FizzBuzz};