import * as fs from "fs";

let fileName: string = process.argv[2];
let wrap : number = +process.argv[3];

function wrapText(): void {
    let fileContents : string = readFile();
    let arrayOfText : Array<string> = fileContents.split(" ");
    let arraySize : number = arrayOfText.length;
    let outputString: string = processArray(arrayOfText, arraySize);
    writeFile(outputString);
}

function readFile(): string {
    const text = fs.readFileSync(fileName, 'utf8');
    return text;
}

function processArray(arrayOfText: Array<string>, arraySize: number): string {
    let outputString: string = "";

    for (let i = 0; i < arraySize; i++) {
        outputString += arrayOfText[i] + " ";
        if (i != 0 && (i+1) % wrap == 0) {
            outputString += '\n';
        }
    }
    return outputString;
}

function writeFile(stringToWrite: string): void {
    fs.writeFileSync(fileName, stringToWrite);
}

wrapText();